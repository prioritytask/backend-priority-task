# Backend - Priority Task

### Generate migrations
`python manage.py makemigrations`

### Apply migrations
`python manage.py migrate`

### Run the server
Beware of the automatic reload process. The automatic reload process receives the same arguments and goes through the same initialization process as the original. Disable the automatic reload process by adding the following argument to your runtime configuration: `--noreload`  

`python manage.py runserver --noreload`