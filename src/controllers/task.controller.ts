import {TaskService} from "../services/task.service";
import {Task} from "../entities/task.entity";
import {NextFunction, Request, Response} from "express";

export class TaskController {

    private taskService: TaskService = new TaskService();

    create(req: Request, res: Response, next: NextFunction): void {
        this.taskService.create(req.body)
            .then((task: Task) => {
                return res.status(201).json(task);
            });
    }

    findAll(req: Request, res: Response, next: NextFunction): void {
        this.taskService.findAll()
            .then((result: Array<Task>) => {
                return res.status(200).json(result);
            });
    }

    findAllByUser(req: Request, res: Response, next: NextFunction) {

        if (!req.params.user) {
            return res.status(404).json("User parameter not found");
        }

        this.taskService.findAllByUser(req.params.user)
            .then((list: Array<Task>) => {
                return res.status(200).json(list);
            })
    }


}