// src/data-source.ts
import { DataSource } from 'typeorm';
import {Task} from "./entities/task.entity";
import {User} from "./entities/user.entity";

export const AppDataSource: DataSource = new DataSource({
    type: "postgres",
    host: "localhost",
    port: 5432,
    username: "postgres",
    password: "postgres",
    database: "test",
    synchronize: true,
    logging: false,
    entities: [Task, User],
    migrations: [],
    subscribers: [],
})
