import {AppDataSource} from "../data-source";
import {Task} from "../entities/task.entity";
import {Repository} from "typeorm";

export class TaskService {

    private taskRepository: Repository<Task> = AppDataSource.getRepository(Task);

    async findAll(): Promise<Task[]> {
        return this.taskRepository.find();
    }

    async create(res: Task): Promise<Task> {

        return this.taskRepository.create(res);
    }

    async findAllByUser(assignedTo: any): Promise<Array<Task>> {

        return this.taskRepository.findBy({
            assignedTo
        })
    }
}