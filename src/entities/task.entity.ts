import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Status} from "../models/Status";
import {User} from "./user.entity";

@Entity({name: 'task'})
export class Task {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nom: string;

    @Column()
    description: string;

    @Column()
    status: Status;

    @ManyToOne(() => User, (user: User) => user.tasks)
    assignedTo: User;

    @Column()
    date: Date;
}