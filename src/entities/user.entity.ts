import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {Task} from "./task.entity";

@Entity({name: 'user'})
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nom: string;

    @Column()
    prenom: string;

    @Column()
    mail: string;

    @OneToMany(() => Task, (task: Task) => task.assignedTo)
    tasks: Array<Task>;

}