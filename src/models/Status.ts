export enum Status {
    EN_COURS = "En cours",
    DONE = "Fini",
    TODO = "A faire"
}