// src/app.ts
import express, {Express} from 'express';
import "reflect-metadata";
import { AppDataSource } from './data-source';
import taskRoute from "./routes/task.route";

const app: Express = express();
const port: number = 8080;

app.use(express.json()); // Pour le parsing des requêtes JSON

AppDataSource.initialize().then((): void => {

    // Ajouter les routes ici
    app.use(taskRoute);

    app.listen(port, (): void => {
        console.log(`Server running at http://localhost:${port}`);
    });

}).catch((error: any): void => {
    console.log("Erreur lors de l'initialisation de la DataSource", error)
});
