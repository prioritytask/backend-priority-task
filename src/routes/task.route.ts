import {Router} from "express";
import {TaskController} from "../controllers/task.controller";

const router: Router = Router();
const controller: TaskController = new TaskController();

router
    .route('/tasks')
    .get(controller.findAll.bind(controller))
    .post(controller.create.bind(controller));

router
    .route('/tasks/:user')
    .get(controller.findAllByUser.bind(controller))

export default router;